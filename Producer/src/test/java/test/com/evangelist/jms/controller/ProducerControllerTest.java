package test.com.evangelist.jms.controller;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.evangelist.jms.producer.controller.ProducerController;
import com.evangelist.jms.producer.model.Vendor;

public class ProducerControllerTest {
	private Vendor vendor;
	private Model model;
	private ProducerController producerController;
	private ApplicationContext context;

	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("spring/application-config.xml");
		producerController = (ProducerController) context.getBean("producerController");
		vendor = new Vendor();
		vendor.setVendorName("UPS");
		vendor.setFirstName("Genaldy");
		vendor.setLastName("Jiwari");
		vendor.setAddress("Jl. Rajawali 1");
		vendor.setCity("Bekasi");
		vendor.setZipCode("178334");
		vendor.setEmail("genaldy@juke.com");
		vendor.setPhoneNumber("120-88937-345");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRenderVendorPage() {
		assertEquals("index", producerController.renderVendorPage(vendor, model));
	}

	@Test
	public void testProcessRequest() {
		ModelAndView mv = producerController.processRequest(vendor, model);
		assertEquals("index", mv.getViewName());
	}

}
